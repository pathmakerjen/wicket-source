# Usage

Customizations that are important to WicketSource working:

1) WicketApplication.java got an additional line in init(). 
   Suggested that you wrap it in an "if (isDevEnvironment()) { }" for performance.
   
        WicketSource.configure(this);
  
2) pom.xml got the addition of wicketsource:

		<dependency>
			<groupId>com.github.jennybrown8.wicketsource</groupId>
			<artifactId>wicketsource</artifactId>
			<version>7.4.0.1</version>
		</dependency>
		
3) The home page class and html were customized merely to give the user 
   something worthwhile to click on (tags with wicketsource="" attributes).


# Development

First, upgrade the wicket-source project as needed.  Them from
that directory, after running `mvn package` so the jar is built, 
install the jar file into your local maven repo.

Install the newly updated wicket-source jar into your local Maven repository 
(typically .m2 in your home folder) as follows:

    mvn install:install-file \
       -Dfile=<path-to-file> \
       -DgroupId=<group-id> \
       -DartifactId=<artifact-id> \
       -Dversion=<version> \
       -Dpackaging=<packaging> \
       -DgeneratePom=true

so for instance:

    mvn install:install-file \
       -Dfile=./target/wicket-source-9.0.1-SNAPSHOT.jar \
       -DgroupId=com.github.jennybrown8.wicket-source \
       -DartifactId=wicket-source \
       -Dversion=9.0.1-SNAPSHOT \
       -Dpackaging=jar \
       -DgeneratePom=true

Then rebuild the demo app, which should automatically pick up the dependency off
of local m2:

    mvn compile

Then you'll want to start up jetty to run the app.  This is most easily done through
the Eclipse IDE, rather than through maven.  Remember to use "Run as... Maven build" to do a compile
for dependencies, and "Maven... Update Project" to tell Eclipse about the new dependencies.

# Publishing

Once the jar tests out via the web app working fine, then it's okay to publish the jar up
to Sonatype, although that also requires setting up the private key for jar signing.



